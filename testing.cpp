//
// Created by nhatminh2947 on 5/27/19.
//

#include <iostream>
#include <iterator>
#include <algorithm>
#include "Board.h"
#include "Human.h"
#include "Player.h"
#include "Game.h"
#include <map>

static void show_usage(std::string name) {
//    std::cerr << "Usage: " << argv[0] << " <option(s)> SOURCES"
//              << "Options:\size_"
//              << "\t-h,--help\t\tShow this help message\size_"
//              << "\t-d,--destination DESTINATION\tSpecify the destination path"
//              << std::endl;
}

int main(int argc, char *argv[]) {
    std::cout << "AI PROJECT 2019 DEMO: ";
    std::copy(argv, argv + argc, std::ostream_iterator<const char *>(std::cout, " "));
    std::cout << std::endl << std::endl;

    AI player1(Algorithm::UCT);
    AI player2;

    player1.setName("AI - 1");
    player2.setName("AI - 2");

    player1.setPiece(Piece::R);
    player2.setPiece(Piece::B);

    int size = 4;
    int seed = time(nullptr);
    std::string order = "f";
    Game game;

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if (para.find("--size=") == 0 || para.find("-n=") == 0) {
            size = std::stoi(para.substr(para.find('=') + 1));
        } else if (para.find("--piece=") == 0 || para.find("-p=") == 0) {
            std::string piece = para.substr(para.find('=') + 1);
            std::transform(piece.begin(), piece.end(), piece.begin(), ::tolower);

            if (piece == "o" || piece == "blue" || piece == "b") {
                player1.setPiece(Piece::B);
                player2.setPiece(Piece::R);
            } else if (piece == "x" || piece == "red" || piece == "r") {
                player1.setPiece(Piece::R);
                player2.setPiece(Piece::B);
            } else {
                std::cout << "Invalid parameter." << std::endl;
                return 0;
            }
        } else if (para.find("--order=") == 0 || para.find("-o=") == 0) {
            order = para.substr(para.find('=') + 1);
        } else if (para.find("--seed=") == 0) {
            seed = std::stoi(para.substr(para.find('=') + 1));
        }
    }

    game.setSeed(seed);
    game.setBoardSize(size);

    if (order == "first" || order == "f") {
        game.setPlayFirst(true);
    } else if (order == "second" || order == "s") {
        game.setPlayFirst(false);
    }

    std::cout << player1.toString() << " vs " << player2.toString() << std::endl;
    std::cout << "Initial board: " << std::endl;
    std::cout << game.getBoard().toString() << std::endl;

    std::map<Piece, int> counter;
    for (int i = 0; i < 1000; ++i) {
        Game curent_game = game;
        while (!curent_game.end()) {
            Player &player = curent_game.takeTurn(player1, player2);
            std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
            Action move = player.makeMove(curent_game.getBoard());
            std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
            std::cout << "Making move time: " << duration << "ms" << std::endl;
            std::cout << player.toString() << " " << move.toString() << std::endl;

            if (!curent_game.apply(move)) {
                break;
            }
            std::cout << curent_game.getBoard().toString() << std::endl;
        }

        Player player = curent_game.getWinner(player1, player2);

        counter[player.getPiece()]++;

        if (player.getPiece() == Piece::EMPTY) {
            std::cout << "The game is draw" << std::endl;
        } else {
            std::cout << player.toString() << " wins the game" << std::endl;
        }
    }

    std::cout << "Statistic" << std::endl;
    std::cout << "Winning rate of R:\t" << counter[Piece::R] / 1000.0 << std::endl;
    std::cout << "Drawing rate of OX:\t" << counter[Piece::EMPTY] / 1000.0 << std::endl;
    std::cout << "Winning rate of B:\t" << counter[Piece::B] / 1000.0 << std::endl;

    return 0;
}