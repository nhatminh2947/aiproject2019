//
// Created by nhatminh2947 on 5/11/19.
//


#ifndef AISPRING2019_BOARD_H
#define AISPRING2019_BOARD_H

#include "Action.h"
#include <vector>
#include <set>
#include <cassert>
#include <random>

enum class Piece {
    EMPTY = ' ',
    R = 'R',
    B = 'B'
};

class Board {
private:
    Piece cell_[20][20]{};
    int size_;
    int n_X_, n_O_;
    int seed_;
    std::mt19937 mt;

    void randomGenerate(int n);

    void crossGenerate(int n);

    bool isInside(Action pos);

    bool isRemovable(Action pos);

    int countConnection(Action pos);

public:

    explicit Board(int size = 4);

    int remove(Action move);

    void setBoardSize(int size);

    void sideEffect(Action move);

    Piece whoWin();

    Piece getPiece(int i);

    Piece getPiece(int i, int j) const;

    Piece getPiece(Action move);

    Piece getPiece(Action move) const;

    std::string toString();

    int size();

    int countRemainingPiece();

    int countPiece(Piece piece);

    double evaluate(Piece maxPlayer, Piece minPlayer);

    void setSeed(int seed);

    bool isTerminal();

    double reward(Piece piece);

    static Piece getOpponentPiece(Piece piece);

    std::vector<Action> getActions(Piece piece);

    std::vector<Action> getActions(Piece piece) const;

    void saveBoard(std::string file_path);

    void setTile(int i, int j, char piece);

    int size() const;
};


#endif //AISPRING2019_BOARD_H
