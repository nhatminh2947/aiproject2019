//
// Created by nhatminh2947 on 5/12/19.
//

#include <fstream>
#include "Game.h"

bool Game::end() {
    return board.isTerminal();
}

Game::Game(int n, bool isFirst) {
    this->n_step = 0;
    this->board = Board(n);
    this->player1_first_ = isFirst;
    this->last_played_piece = Piece::EMPTY;
}

Player &Game::takeTurn(Player &player1, Player &player2) {
    return player1_first_ ? this->n_step % 2 == 0 ? player1 : player2 : this->n_step % 2 == 0 ? player2 : player1;
}

void Game::setBoardSize(int size) {
    this->board.setBoardSize(size);
}

void Game::setPlayFirst(bool isFirst) {
    this->player1_first_ = isFirst;
}

Board Game::getBoard() {
    return this->board;
}

Piece Game::whoWin() {
    if (this->board.countPiece(Piece::R) == 0) {
        return Piece::R;
    }

    if (this->board.countPiece(Piece::B) == 0) {
        return Piece::B;
    }

    return Piece::EMPTY;
}

bool Game::apply(Action move) {
    if (this->board.getPiece(move) == Piece::EMPTY || this->board.getPiece(move) == last_played_piece) {
        return false;
    }

    if (this->board.remove(move) == -1) {
        return false;
    }

    this->board.sideEffect(move);
    this->n_step++;
    this->game_actions.push_back(move);
    last_played_piece = this->board.getPiece(move);

    return true;
}

Player Game::getWinner(const Player &player1, const Player &player2) {
    if (this->board.countRemainingPiece() == 0) {
        return Player("Draw", PlayerType::HUMAN, Piece::EMPTY);
    }

    if (this->board.countPiece(player1.getPiece()) == 0) {
        return player1;
    }

    if (this->board.countPiece(player2.getPiece()) == 0) {
        return player2;
    }

    return Player("No One", PlayerType::HUMAN, Piece::EMPTY);
}

bool Game::hasWinner(Board board) {
    if (board.countPiece(Piece::R) == 0) {
        return true;
    }

    if (board.countPiece(Piece::B) == 0) {
        return true;
    }

    return false;
}

void Game::setSeed(int seed) {
    board.setSeed(seed);
}

void Game::load(std::string board_config) {
    int size;
    std::ifstream load;
    load.open(board_config);
    load >> size;

    this->setBoardSize(size);
    char value;

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            load >> value;
            this->board.setTile(i, j, value);
        }
    }

    load.close();
}

void Game::exportGameActions(Player player1, const std::string& export_file) {
    std::ofstream ofstream;
    ofstream.open(export_file, std::ios::out | std::ios::trunc);

    Piece current_piece = player1.getPiece();
    if(!player1_first_) {
        current_piece = Board::getOpponentPiece(current_piece);
    }

    for (auto &game_action : this->game_actions) {
        ofstream << static_cast<char>(current_piece) << " " << game_action.toString() << std::endl;
        current_piece = Board::getOpponentPiece(current_piece);
    }

    ofstream.close();
}
