//
// Created by nhatminh2947 on 5/12/19.
//

#include "Human.h"

Action Human::makeMove(const Board &board) {
    std::string symbol = this->getPiece() == Piece::R ? "R" : "B";

    int x, y;
    std::cout << "HUMAN ( " + symbol + " ) Action: ";
    std::cin >> x >> y;

    while (board.getPiece(x, y) != this->getPiece()) {
        std::cout << Action(x, y).toString() << " is an invalid pos_. Please select another position." << std::endl;
        std::cout << "HUMAN ( " + symbol + " ) Action: ";
        std::cin >> x >> y;
    }

    return {x, y};
}

Human::Human(std::string name, PlayerType type, Piece piece) : Player(name, type, piece) {

}