//
// Created by nhatminh2947 on 5/12/19.
//

#ifndef AISPRING2019_GAME_H
#define AISPRING2019_GAME_H


#include "Player.h"
#include "Human.h"
#include "AI.h"

class Game {
private:
    bool player1_first_;
    int n_step;
    Board board;
    Piece last_played_piece;
    std::vector<Action> game_actions{};

public:
    explicit Game(int n = 4, bool isFirst = true);

    void setBoardSize(int size);

    void setPlayFirst(bool isFirst);

    Player &takeTurn(Player &player1, Player &player2);

    bool end();

    Board getBoard();

    bool apply(Action move);

    Player getWinner(const Player &player1, const Player &player2);

    static bool hasWinner(Board board);

    Piece whoWin();

    void setSeed(int seed);

    void load(std::string board_config);

//    void exportGameActions(const std::string& export_file = "moves.log");

    void exportGameActions(Player player1, const std::string &export_file = "moves.log");
};


#endif //AISPRING2019_GAME_H
