//
// Created by nhatminh2947 on 5/12/19.
//

#ifndef AISPRING2019_HUMAN_H
#define AISPRING2019_HUMAN_H


#include "Player.h"

class Human : public Player {
public:
    Action makeMove(const Board &board) override;

    explicit Human(std::string name = "Human", PlayerType type = PlayerType::HUMAN, Piece piece = Piece::B);
};


#endif //AISPRING2019_HUMAN_H
