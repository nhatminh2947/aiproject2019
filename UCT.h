//
// Created by nhatminh2947 on 5/24/19.
//

#ifndef AIPROJECT2019_UCT_H
#define AIPROJECT2019_UCT_H


#include <cstdio>
#include <memory>
#include <set>
#include "Board.h"
#include "Game.h"

class UCT {
    class Node {
    private:
        double score_;
        int n_simulations_;
        double value_;
        Piece player_just_moved_;
        std::shared_ptr<Action> action_;
        std::weak_ptr<Node> parent_;
        std::vector<std::shared_ptr<Action>> untried_actions_{};
        std::vector<std::shared_ptr<Node>> expanded_children_{};
        std::mt19937 mt;

    public:
        Node(const Board &board, std::shared_ptr<Action> action, const std::shared_ptr<Node> &parent, Piece player_just_moved,
             bool generateAllActions = true);

        std::shared_ptr<Node> selectBestChild(Piece root_piece, double c = sqrt(2));

        std::shared_ptr<Node> getParent();

        std::shared_ptr<Action> getAction();

        Piece getPieceOfPlayerJustMoved();

        std::shared_ptr<Node> addChild(int move_id, const std::shared_ptr<Node> &parent, const Board &board);

        bool hasActionToTry();

        void update(double d);

        std::shared_ptr<Action> getUntriedAction(int id);

        int randomUntriedActionId();

        bool hasChildren();

        double getUCTValue(double c = sqrt(2));

        int getNSimulation();

        ~Node();
    };

public:
    std::shared_ptr<Action> search(Board board, Piece root_piece, int iterations);

    void search(Board s0, Piece root_piece, Action &result, double time);

    std::shared_ptr<Action> search(Board board, Piece player_just_moved, double time_in_seconds = 3000);

    void iterate(const Board &current_board, std::shared_ptr<Node> rootNode);
};


#endif //AIPROJECT2019_UCT_H
