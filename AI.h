//
// Created by nhatminh2947 on 5/11/19.
//

#ifndef AISPRING2019_AI_H
#define AISPRING2019_AI_H

#include "Player.h"
#include "Action.h"
#include "Board.h"
#include <string>
#include <cmath>
#include <vector>
#include <chrono>

enum Algorithm {
    Minimax = 0,
    Alphabeta = 1,
    UCT = 2
};

class Node {
private:
    double value_;
    Action move_;
public:
    Node(double d, Action move);

    double getValue();

    Action getAction();

    void setMove(Action move);

    void setValue(double value);

};

class AI : public Player {
private:

    Node minimax(Board board, int depth, Piece maximizingPlayer, Piece minimizingPlayer, bool maximizing);

    Node alphabeta(Board board, int depth, Piece maximizingPlayer, Piece minimizingPlayer, double alpha, double beta,
                   bool maximizing);

    int findDepth(Board board);

    double decay(int x);

    Algorithm algorithm_;

public:
    explicit AI(Algorithm algorithm = Algorithm::Alphabeta, Piece piece = Piece::R);

    Action makeMove(const Board &board) override;
};


#endif //AISPRING2019_AI_H
