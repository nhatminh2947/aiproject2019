//
// Created by nhatminh2947 on 5/12/19.
//

#ifndef AISPRING2019_PLAYER_H
#define AISPRING2019_PLAYER_H

#include <string>
#include "Action.h"
#include "Board.h"
#include <cstdio>
#include <iostream>

enum class PlayerType {
    HUMAN,
    AI
};

class Player {
    std::string name_;
    PlayerType type_;
    Piece piece_;

public:
    Player(std::string name, PlayerType type, Piece piece);

    virtual Action makeMove(const Board &board);

    void setPiece(Piece piece);

    void setName(std::string name);

    Piece getPiece() const;

    std::string toString();

};


#endif //AISPRING2019_PLAYER_H
