//
// Created by nhatminh2947 on 5/11/19.
//

#ifndef AISPRING2019_MOVE_H
#define AISPRING2019_MOVE_H

#include <iostream>

class Action {
public:
    int x_{};
    int y_{};

    Action();

    Action(int x, int y);

    std::string toString();

    bool operator<(const Action &rhs) const;

    bool operator==(const Action &rhs) const;
};


#endif //AISPRING2019_MOVE_H
