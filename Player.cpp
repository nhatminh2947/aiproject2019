#include <utility>

//
// Created by nhatminh2947 on 5/12/19.
//

#include "Player.h"
#include <string>

Player::Player(std::string name, PlayerType type, Piece piece) {
    this->name_ = std::move(name);
    this->type_ = type;
    this->piece_ = piece;
}

void Player::setPiece(Piece piece) {
    this->piece_ = piece;
}

Piece Player::getPiece() const {
    return this->piece_;
}

Action Player::makeMove(const Board &board) { return {}; }

std::string Player::toString() {
    return this->name_ + " ( " + (this->piece_ == Piece::R ? "R" : "B") + " )";
}

void Player::setName(std::string name) {
    this->name_ = std::move(name);
}
