//
// Created by nhatminh2947 on 5/11/19.
//
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <sstream>
#include <fstream>
#include "Board.h"

int dx[] = {-1, 0, 0, 1};
int dy[] = {0, -1, 1, 0};

bool Board::isInside(Action pos) {
    return 0 <= pos.x_ && pos.x_ < size_ && 0 <= pos.y_ && pos.y_ < size_;
}

bool Board::isRemovable(Action pos) {
    return (cell_[pos.x_][pos.y_] != Piece::EMPTY);
}

Board::Board(int size) {
    this->n_X_ = this->n_O_ = size * size / 2;
    this->size_ = size;
    this->seed_ = time(nullptr);
    randomGenerate(size);
}

void Board::setBoardSize(int size) {
    this->n_X_ = this->n_O_ = size * size / 2;
    this->size_ = size;
    randomGenerate(size);
}

void Board::crossGenerate(int n) {
    int black = 0;
    int white = 0;
    int limit = n * n / 2;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; ++j) {
            Piece piece;
            if ((i + i * n + j) % 2 == 0) {
                piece = Piece::R;
            } else {
                piece = Piece::B;
            }

            if (black == limit) {
                piece = Piece::B;
            }

            if (white == limit) {
                piece = Piece::R;
            }

            this->cell_[i][j] = piece;

            if (piece == Piece::R) {
                black++;
            } else {
                white++;
            }
        }
    }
}

void Board::randomGenerate(int n) {
    std::uniform_int_distribution<int> urd(0, 1);

    int black = 0;
    int white = 0;
    int limit = n * n / 2;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; ++j) {
            Piece piece;
            if (urd(this->mt) % 2 == 0) {
                piece = Piece::R;
            } else {
                piece = Piece::B;
            }

            if (black == limit) {
                piece = Piece::B;
            }

            if (white == limit) {
                piece = Piece::R;
            }

            this->cell_[i][j] = piece;

            if (piece == Piece::R) {
                black++;
            } else {
                white++;
            }
        }
    }
}

int Board::remove(Action move) {
    if (isInside(move) && isRemovable(move)) {
        if (this->cell_[move.x_][move.y_] == Piece::B) {
            this->n_O_--;
        } else if (this->cell_[move.x_][move.y_] == Piece::R) {
            this->n_X_--;
        }

        this->cell_[move.x_][move.y_] = Piece::EMPTY;


        return 0;
    }
    return -1;
}

std::string Board::toString() {
    std::stringstream out;

    out << "   ";
    for (int j = 0; j < size_; ++j) {
        out << "   " << j << "  ";
    }
    out << " " << std::endl;
    out << std::endl;

    out << "   ";
    for (int j = 0; j < 6 * size_; ++j) {
        out << "█";
    }
    out << "█" << std::endl;

    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_ + 1; ++j) {
            if (j) {
                out << "  ";
            }
            out << "   █";
        }
        out << std::endl;

        out << i;
        for (int j = 0; j < size_; ++j) {
            std::string p = " ";
            if (cell_[i][j] == Piece::R) {
//                p = "\u2718";
                p = "R";
            }
            if (cell_[i][j] == Piece::B) {
//                p = "\u25cb";
                p = "B";
            }

            out << "  █  " << p;
        }
        out << "  █" << std::endl;

        for (int j = 0; j < size_ + 1; ++j) {
            if (j) {
                out << "  ";
            }
            out << "   █";
        }
        out << std::endl;

        out << "   ";
        for (int j = 0; j < 6 * size_; ++j) {
            out << "█";
        }
        out << "█" << std::endl;
    }

    return out.str();
}

void Board::sideEffect(Action move) {
    for (int i = 0; i < 4; ++i) {
        int x = move.x_ + dx[i];
        int y = move.y_ + dy[i];

        Action m(x, y);

        if (isInside(m)) {
            int connections = countConnection(m);
            if (connections < 3) {
                remove(m);
            }
        }
    }
}

int Board::countConnection(Action pos) {
    int count = 0;
    for (int i = 0; i < 4; ++i) {
        int x = pos.x_ + dx[i];
        int y = pos.y_ + dy[i];

        if (!isInside(pos) || this->cell_[x][y] != Piece::EMPTY) {
            count++;
        }
    }

    return count;
}

double Board::evaluate(Piece maxPlayer, Piece minPlayer) {
    return -countPiece(maxPlayer) + countPiece(minPlayer);
}

Piece Board::getPiece(int i) {
    int row = i / this->size_;
    int col = i % this->size_;

    return this->getPiece(row, col);
}

Piece Board::getPiece(int i, int j) const {
    return this->cell_[i][j];
}

int Board::size() {
    return this->size_;
}

int Board::size() const {
    return this->size_;
}

Piece Board::whoWin() {
    if (this->n_X_ == 0) {
        return Piece::R;
    }

    if (this->n_O_ == 0) {
        return Piece::B;
    }

    return Piece::EMPTY;
}

int Board::countRemainingPiece() {
    return this->n_X_ + this->n_O_;
}

Piece Board::getPiece(Action move) {
    return getPiece(move.x_, move.y_);
}

int Board::countPiece(Piece piece) {
    if (piece == Piece::B) {
        return n_O_;
    }

    if (piece == Piece::R) {
        return n_X_;
    }

    return INT32_MAX;
}

void Board::setSeed(int seed) {
    this->seed_ = seed;
    this->mt.seed(seed);
}

bool Board::isTerminal() {
    return (this->n_O_ == 0) || (this->n_X_ == 0);
}

double Board::reward(Piece piece) {
    if (this->n_X_ == 0 && this->n_O_ == 0) {
        return 0.5;
    }

    if (countPiece(piece) == 0) {
        return 1;
    }

    return 0;
}

Piece Board::getOpponentPiece(Piece piece) {
    assert(piece != Piece::EMPTY);

    if (piece == Piece::R) {
        return Piece::B;
    }

    return Piece::R;
}

Piece Board::getPiece(Action move) const {
    return getPiece(move.x_, move.y_);
}

std::vector<Action> Board::getActions(Piece piece) {
    std::vector<Action> all_actions;
    for (int i = 0; i < this->size_; ++i) {
        for (int j = 0; j < this->size_; ++j) {
            if (this->getPiece(i, j) == piece) {
                all_actions.emplace_back(i, j);
            }
        }
    }

    return all_actions;
}

std::vector<Action> Board::getActions(Piece piece) const {
    std::vector<Action> all_actions;
    for (int i = 0; i < this->size_; ++i) {
        for (int j = 0; j < this->size_; ++j) {
            if (this->getPiece(i, j) == piece) {
                all_actions.emplace_back(i, j);
            }
        }
    }

    return all_actions;
}

void Board::saveBoard(std::string file_path) {
    std::fstream fout;
    fout.open(file_path, std::ios::out | std::ios::trunc);
    fout << this->size() << std::endl;

    for (int i = 0; i < this->size(); ++i) {
        for (int j = 0; j < this->size(); ++j) {
            fout << (this->cell_[i][j] == Piece::R ? 'R' : 'B') << " ";
        }
        fout << std::endl;
    }
    fout.close();
}

void Board::setTile(int i, int j, char piece) {
    this->cell_[i][j] = (piece == 'R' ? Piece::R : Piece::B);
}
