//
// Created by nhatminh2947 on 5/11/19.
//

#include "AI.h"
#include "Game.h"
#include "UCT.h"

Action AI::makeMove(const Board &board) {
    Piece opponent_piece = Board::getOpponentPiece(this->getPiece());
    if (this->algorithm_ == Algorithm::Alphabeta) {
        int depth = findDepth(board);
        std::cout << "Current depth: " << depth << std::endl;
        Node node = alphabeta(board, depth, this->getPiece(), opponent_piece, INT64_MIN, INT64_MAX, true);
        std::cout << "Node evaluation: " << node.getValue() << std::endl;

        return node.getAction();
    } else if (this->algorithm_ == Algorithm::Minimax) {
        int depth = findDepth(board);
        std::cout << "Current depth: " << depth << std::endl;
        Node node = minimax(board, depth, this->getPiece(), opponent_piece, true);

        return node.getAction();
    } else if (this->algorithm_ == Algorithm::UCT) {
        class UCT uct;
        return *uct.search(board, Board::getOpponentPiece(this->getPiece()), 10.00000);
    }

    return {-1, -1};
}

AI::AI(Algorithm algorithm, Piece piece) : Player("AI", PlayerType::AI, piece), algorithm_(algorithm) {

}

Node AI::alphabeta(Board board, int depth, Piece maximizingPlayer, Piece minimizingPlayer, double alpha, double beta,
                   bool maximizing) {
    if (depth == 0 || Game::hasWinner(board)) {
        return {board.evaluate(maximizingPlayer, minimizingPlayer), Action(-1, -1)};
    }

    if (maximizing) {
        Node node(INT64_MIN, Action(-1, -1));
        bool cut = false;
        for (int i = 0; i < board.size() && !cut; i++) {
            for (int j = 0; j < board.size() && !cut; j++) {
                if (board.getPiece(i, j) == maximizingPlayer) {
                    Board newBoard = board;
                    newBoard.remove(Action(i, j));
                    newBoard.sideEffect(Action(i, j));

                    Node childNode = alphabeta(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, alpha, beta,
                                               !maximizing);

                    if (childNode.getValue() > node.getValue()) {
                        node.setValue(childNode.getValue());
                        node.setMove(Action(i, j));
                    }

                    alpha = fmax(alpha, node.getValue());
                    if (alpha >= beta) {
                        cut = true;
                    }
                }
            }
        }

        return node;
    } else {
        Node node(INT64_MAX, Action(-1, -1));
        bool cut = false;
        for (int i = 0; i < board.size() && !cut; i++) {
            for (int j = 0; j < board.size() && !cut; j++) {
                if (board.getPiece(i, j) == minimizingPlayer) {
                    Board newBoard = board;
                    newBoard.remove(Action(i, j));
                    newBoard.sideEffect(Action(i, j));

                    Node childNode = alphabeta(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, alpha, beta,
                                               !maximizing);

                    if (childNode.getValue() < node.getValue()) {
                        node.setValue(childNode.getValue());
                        node.setMove(Action(i, j));
                    }

                    beta = fmin(beta, node.getValue());
                    if (alpha >= beta) {
                        cut = true;
                    }
                }
            }
        }

        return node;
    }
}

Node AI::minimax(Board board, int depth, Piece maximizingPlayer, Piece minimizingPlayer, bool maximizing) {
    if (depth == 0 || Game::hasWinner(board)) {
        return {board.evaluate(maximizingPlayer, minimizingPlayer), Action(-1, -1)};
    }

    if (maximizing) {
        Node node(INT64_MIN, Action(-1, -1));
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board.size(); j++) {
                if (board.getPiece(i, j) == maximizingPlayer) {
                    Board newBoard = board;
                    newBoard.remove(Action(i, j));
                    if (Game::hasWinner(newBoard)) {
                        Node childNode = minimax(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, !maximizing);

                        if (childNode.getValue() > node.getValue()) {
                            node.setValue(childNode.getValue());
                            node.setMove(Action(i, j));
                        }
                    } else {
                        newBoard.sideEffect(Action(i, j));
                        Node childNode = minimax(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, !maximizing);

                        if (childNode.getValue() > node.getValue()) {
                            node.setValue(childNode.getValue());
                            node.setMove(Action(i, j));
                        }
                    }
                }
            }
        }

        return node;
    } else {
        Node node(INT64_MAX, Action(-1, -1));

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board.size(); j++) {
                if (board.getPiece(i, j) == minimizingPlayer) {
                    Board newBoard = board;
                    newBoard.remove(Action(i, j));
                    if (Game::hasWinner(newBoard)) {
                        Node childNode = minimax(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, !maximizing);

                        if (childNode.getValue() < node.getValue()) {
                            node.setValue(childNode.getValue());
                            node.setMove(Action(i, j));
                        }
                    } else {
                        newBoard.sideEffect(Action(i, j));
                        Node childNode = minimax(newBoard, depth - 1, maximizingPlayer, minimizingPlayer, !maximizing);

                        if (childNode.getValue() < node.getValue()) {
                            node.setValue(childNode.getValue());
                            node.setMove(Action(i, j));
                        }
                    }
                }
            }
        }

        return node;
    }
}

int AI::findDepth(Board board) {
    int x = board.countRemainingPiece();
    double d = decay(x);
    int depth = int(floor(d)) / 2 * 2 + 1;
    std::cout << "Number of remaining Piece: " << x << std::endl;
    return depth;
}

double AI::decay(int x) {
    return 10*exp(-0.03 * (x-15))+5;
}

double Node::getValue() {
    return this->value_;
}

Action Node::getAction() {
    return this->move_;
}

void Node::setMove(Action move) {
    this->move_ = move;
}

void Node::setValue(double value) {
    this->value_ = value;
}

Node::Node(double d, Action move) {
    this->value_ = d;
    this->move_ = move;
}
