//
// Created by nhatminh2947 on 5/11/19.
//

#include <sstream>
#include "Action.h"

Action::Action(int x, int y) {
    this->x_ = x;
    this->y_ = y;
}

Action::Action() = default;

std::string Action::toString() {
    std::stringstream out;
    out << "(" << this->x_ << ", " << this->y_ << ")";
//    out << "(" << this->y_ << ", " << this->x_ << ")";
    return out.str();
}

bool Action::operator<(const Action &rhs) const {
    if (this->x_ == rhs.x_) return this->y_ < rhs.y_;
    return this->x_ < rhs.x_;
}

bool Action::operator==(const Action &rhs) const {
    return this->x_ == rhs.x_ && this->y_ == rhs.y_;
}