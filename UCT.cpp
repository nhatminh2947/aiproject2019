#include <utility>

#include <utility>

#include <utility>

#include <utility>

//
// Created by nhatminh2947 on 5/24/19.
//

#include <cmath>
#include "UCT.h"

std::shared_ptr<UCT::Node> UCT::Node::getParent() {
    return this->parent_.lock();
}

std::shared_ptr<Action> UCT::Node::getAction() {
    return this->action_;
}

Piece UCT::Node::getPieceOfPlayerJustMoved() {
    return this->player_just_moved_;
}

UCT::Node::Node(const Board &board, std::shared_ptr<Action> action, const std::shared_ptr<Node> &parent, Piece player_just_moved,
                bool generateAllAction)
        : score_(0), n_simulations_(0), value_(0), player_just_moved_(player_just_moved), action_(std::move(action)), parent_(parent) {

    std::random_device rd;
    this->mt = std::mt19937(rd());

    if (generateAllAction) {
        std::vector<Action> all_actions;
        for (int i = 0; i < board.size(); ++i) {
            for (int j = 0; j < board.size(); ++j) {
                if (board.getPiece(i, j) == Board::getOpponentPiece(player_just_moved)) {
                    untried_actions_.push_back(std::make_shared<Action>(i, j));
                }
            }
        }
    }
}

void UCT::Node::update(double d) {
    this->n_simulations_++;
    this->score_ += d;
}

bool UCT::Node::hasActionToTry() {
    return !untried_actions_.empty();
}

bool UCT::Node::hasChildren() {
    return !this->expanded_children_.empty();
}

std::shared_ptr<UCT::Node> UCT::Node::selectBestChild(Piece root_piece, double c) {
    std::shared_ptr<Node> best_child;

    double max_ucb = INT64_MIN;
    for (const auto &expandedChild : this->expanded_children_) {
//        if (c == 0) {
//            std::cout << expandedChild->getAction()->toString() << " " << expandedChild->getUCTValue(c);
//            std::cout << " (" << expandedChild->n_simulations_ << "/" << expandedChild->parent_.lock()->n_simulations_ << ")" << std::endl;
//        }
        if (max_ucb < expandedChild->getUCTValue(c)) {
            max_ucb = expandedChild->getUCTValue(c);
            best_child = expandedChild;
        }
    }

    return best_child;
}

double UCT::Node::getUCTValue(double c) {
    return 1.0 * this->score_ / this->n_simulations_ +
           c * sqrt(log(this->parent_.lock()->n_simulations_) / this->n_simulations_);
}

int UCT::Node::getNSimulation() {
    return this->n_simulations_;
}

int UCT::Node::randomUntriedActionId() {
    std::uniform_int_distribution<int> dist(0, untried_actions_.size() - 1);
    return dist(mt);
}

std::shared_ptr<Action> UCT::Node::getUntriedAction(int id) {
    return untried_actions_[id];
}

std::shared_ptr<UCT::Node>
UCT::Node::addChild(int move_id, const std::shared_ptr<UCT::Node> &parent, const Board &board) {
    std::shared_ptr<Action> move = untried_actions_[move_id];
    std::shared_ptr<Node> n = std::make_shared<Node>(board, move, parent, Board::getOpponentPiece(parent->getPieceOfPlayerJustMoved()));
    this->untried_actions_[move_id] = this->untried_actions_.back();
    this->untried_actions_.pop_back();
    this->expanded_children_.push_back(n);
    return n;
}

UCT::Node::~Node() = default;

std::shared_ptr<Action> UCT::search(Board board, Piece root_piece, int iterations) {
    std::shared_ptr<Node> root = std::make_shared<Node>(Node(board, std::make_shared<Action>(-1, -1), nullptr,
                                                             root_piece));
    while (iterations--) {
        this->iterate(board, root);
    }

    iterations = 100000;
    while (root->selectBestChild(root_piece, 0)->getUCTValue(0) < 4.9999999 && iterations--) {
        this->iterate(board, root);
        std::shared_ptr<Node> bestChild = root->selectBestChild(root_piece, 0);
        std::cout << "UCT Value: " << bestChild->getUCTValue(0) << std::endl;
    }

    std::shared_ptr<Node> bestChild = root->selectBestChild(root_piece, 0);
    std::cout << "UCT Value: " << bestChild->getUCTValue(0) << std::endl;

    return bestChild->getAction();
}

void UCT::search(Board board, Piece root_piece, Action &result, double time) {
    std::shared_ptr<Node> root = std::make_shared<Node>(
            Node(board, std::make_shared<Action>(-1, -1), nullptr, root_piece));

    double running_time = 0;
    int count = 0;
    while (running_time < time) {
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        this->iterate(board, root);
        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
        running_time += duration;
        count++;
    }

    std::cout << "#running_time: " << running_time << std::endl;
    std::cout << "#iterations: " << count << std::endl;

    result = *root->selectBestChild(root_piece, 0)->getAction();
}

std::shared_ptr<Action> UCT::search(Board board, Piece player_just_moved, double time_in_seconds) {
    std::shared_ptr<Node> root = std::make_shared<Node>(
            Node(board, std::make_shared<Action>(-1, -1), nullptr, player_just_moved));

    double running_time = 0;
    int count = 0;
    while (running_time < time_in_seconds * 1000000000) {
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        this->iterate(board, root);
        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
        running_time += duration;
        count++;
    }

//    int iterations = 100000;
//    while (root->selectBestChild(root_piece, 0)->getUCTValue(0) < 0.5 && iterations--) {
//        this->iterate(board, root);
//        std::shared_ptr<Node> bestChild = root->selectBestChild(root_piece, 0);
//    }
    std::cout << "#iterations: " << count << std::endl;
    std::shared_ptr<Node> bestChild = root->selectBestChild(player_just_moved, 0);
    std::cout << "#UCT Value: " << bestChild->getUCTValue(0) << std::endl;

//    std::shared_ptr<Node> debug_node = root;
//    Board current_board = board;
//    while (!current_board.isTerminal() && !debug_node->hasActionToTry() && debug_node->hasChildren()) {
//        debug_node = debug_node->selectBestChild(player_just_moved, 0);
//        current_board.remove(*debug_node->getAction());
//        current_board.sideEffect(*debug_node->getAction());
//        std::cout << debug_node->getAction()->toString() << "\n";
//    }
//    std::cout << std::endl;

    return bestChild->getAction();
}

void UCT::iterate(const Board &board, std::shared_ptr<UCT::Node> root) {
    std::shared_ptr<Node> node = std::move(root);
    Board current_board = board;
    Piece playerJustMoved = node->getPieceOfPlayerJustMoved();

    //Select
    while (!current_board.isTerminal() && !node->hasActionToTry() && node->hasChildren()) {
        node = node->selectBestChild(playerJustMoved);
        current_board.remove(*node->getAction());
        current_board.sideEffect(*node->getAction());
    }

    // Expand
    if (!current_board.isTerminal() && node->hasActionToTry()) {
        int move_id = node->randomUntriedActionId();
        std::shared_ptr<Action> move = node->getUntriedAction(move_id);

        current_board.remove(*move);
        current_board.sideEffect(*move);
        node = node->addChild(move_id, node, current_board);
    }

    // Rollout
    std::random_device rd;
    std::mt19937 mt(rd());
    Piece player_just_move_piece = node->getPieceOfPlayerJustMoved();
//    std::uniform_int_distribution<int> uid(0, current_board.size());
//
    std::vector<Action> red_moves;
    std::vector<Action> blue_moves;

    std::uniform_int_distribution<int> uid(0, 100);

    for (int i = 0; i < board.size(); ++i) {
        for (int j = 0; j < board.size(); ++j) {
            if (board.getPiece(i, j) == Piece::R) {
                red_moves.emplace_back(i, j);
            } else if (board.getPiece(i, j) == Piece::B) {
                blue_moves.emplace_back(i, j);
            }
        }
    }

    while (!current_board.isTerminal()) {
        if (player_just_move_piece == Piece::R) {
            int id = uid(mt) % blue_moves.size();

            current_board.remove(blue_moves[id]);
            current_board.sideEffect(blue_moves[id]);

            blue_moves[id] = blue_moves.back();
            blue_moves.pop_back();
        } else if (player_just_move_piece == Piece::B) {
            int id = uid(mt) % red_moves.size();

            current_board.remove(red_moves[id]);
            current_board.sideEffect(red_moves[id]);

            red_moves[id] = red_moves.back();
            red_moves.pop_back();
        }

        player_just_move_piece = Board::getOpponentPiece(player_just_move_piece);
    }

    // Backpropagate
//    double reward = current_board.evaluate(AI_piece, Board::getOpponentPiece(AI_piece));

    while (node != nullptr) {
        double reward = current_board.reward(node->getPieceOfPlayerJustMoved());
        node->update(reward);
        node = node->getParent();
    }
}
