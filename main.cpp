#include <iostream>
#include <iterator>
#include <algorithm>
#include "Board.h"
#include "Human.h"
#include "Player.h"
#include "Game.h"

//static void show_usage(std::string name) {
//    std::cerr << "Usage: " << argv[0] << " <option(s)> SOURCES"
//              << "Options:\size_"
//              << "\t-h,--help\t\tShow this help message\size_"
//              << "\t-d,--destination DESTINATION\tSpecify the destination path"
//              << std::endl;
//}

int main(int argc, char *argv[]) {
    std::cout << "AI PROJECT 2019 DEMO: ";
    std::copy(argv, argv + argc, std::ostream_iterator<const char *>(std::cout, " "));
    std::cout << std::endl << std::endl;

    Human player1;
    AI player2(Algorithm::UCT);

    player1.setName("Human");
    player2.setName("UCT");

    player1.setPiece(Piece::R);
    player2.setPiece(Piece::B);


    int size = 4;
    int seed = time(nullptr);
    bool export_moves = false;
    std::string play = "f";
    std::string gen_path;
    std::string board_config;
    std::string piece = "r";

    Game game;

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if (para.find("--seed=") == 0) {
            seed = std::stoi(para.substr(para.find('=') + 1));
        } else if (para.find("--load=") == 0) {
            board_config = para.substr(para.find('=') + 1);
            game.load(board_config);
        } else if (para.find("--gen=") == 0) {
            gen_path = para.substr(para.find('=') + 1);
        } else if (para.find("--export") == 0) {
            export_moves = true;
        }
    }

    std::cout << gen_path << std::endl;
    if (!gen_path.empty()) {
        std::cout << "Select board's size: ";
        std::cin >> size;
        game.setBoardSize(size);
        game.getBoard().saveBoard(gen_path);
        std::cout << "New board was generated at " << gen_path << std::endl;
        return 0;
    }

    game.setSeed(seed);
    if (board_config.empty()) {
        std::cout << "Select board's size: ";
        std::cin >> size;
        game.setBoardSize(size);
    }

    std::cout << "Initial board: " << std::endl;
    std::cout << game.getBoard().toString() << std::endl;

    std::cout << "Pick your piece(R/B): ";
    std::cin >> piece;
    std::transform(piece.begin(), piece.end(), piece.begin(), ::tolower);

    if (piece == "o" || piece == "blue" || piece == "b") {
        player1.setPiece(Piece::B);
        player2.setPiece(Piece::R);
    } else if (piece == "x" || piece == "red" || piece == "r") {
        player1.setPiece(Piece::R);
        player2.setPiece(Piece::B);
    }

    std::cout << "Play first or second(f/s): ";
    std::cin >> play;
    std::transform(play.begin(), play.end(), play.begin(), ::tolower);

    if (play == "first" || play == "f") {
        game.setPlayFirst(true);
    } else if (play == "second" || play == "s") {
        game.setPlayFirst(false);
    }

    std::cout << std::endl;
    std::cout << player1.toString() << " vs " << player2.toString() << std::endl;
    std::cout << std::endl;

    while (!game.end()) {
        Player &player = game.takeTurn(player1, player2);
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        Action move = player.makeMove(game.getBoard());
        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        std::cout << "Making move time: " << duration << "ms" << std::endl;
        std::cout << player.toString() << " " << move.toString() << std::endl;

        if (!game.apply(move)) {
            break;
        }
        std::cout << game.getBoard().toString() << std::endl;
    }

    Player player = game.getWinner(player1, player2);
    if (player.getPiece() == Piece::EMPTY) {
        std::cout << "The game is draw" << std::endl;
    } else {
        std::cout << player.toString() << " wins the game" << std::endl;
    }

    if(export_moves) {
        game.exportGameActions(player1);
        std::cout << "All moves have been save to moves.log" << std::endl;
    }

    return 0;
}
